# Yellow Door

## The project
Yellow Door is an e-commerce website that shows customers the different products that the company sells. 

**To see the project running, go to: [https://react-yellow-door.herokuapp.com/](https://react-yellow-door.herokuapp.com/)**

## The skeleton
The tooling and criteria used for this project were:
* FrontEnd:
    * Create React App
    * Axios

## How to run the app?
On the root folder, `npm start` runs the server.<br>
On client folder, `npm start` runs the React app.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. 

## How does it work?
It's a single-page site. The user will land on the home page and can select a menu item on the left. Then, it will open a sub-menu with the categories in the chosen option, and if the user clicks on one of them, it will show them the available product.
<br>

## Next steps
* **Update to responsive design** there are no designs for tablet or mobile, so the project it's desktop only.
* **Add functionality** there're some buttons without any functionality, so it needs to update.
