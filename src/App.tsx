import React, { useState } from 'react';
import './App.css';
import Sidebar from './components/Sidebar/Sidebar';
import Section from './components/Section/Section';
import Header from './components/Header/Header';
import { SelectCategory } from './App.context';
import Content from './components/Content/Content';

function App() {

  const sections = ["Aberturas", "Equipamiento", "Terminaciones"];

  const [selectedItem, setSelectedItem] = useState<number | null>(null);

  return (

    <SelectCategory.Provider value={ {selectedItem, setSelectedItem} }>
      <div className="App bg-gray-50">
        <div className="flex flex-col h-screen">
          <Header />
          <div className="flex flex-row h-full">
            <Sidebar sections={sections}/>
            {selectedItem && 
              <>
                <Section menu={sections[selectedItem-1]}/>
                <Content />
              </>
            }
          </div>
        </div>
        
      </div>
    </SelectCategory.Provider>
  );
}

export default App;
