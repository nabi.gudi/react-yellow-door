import { createContext, Dispatch, SetStateAction, useState } from "react";

interface AppContextInterface {
  selectedItem: number | null;
  setSelectedItem: Dispatch<SetStateAction<number | null>>
}

export const SelectCategory = createContext<AppContextInterface>(
  {
   selectedItem : null,
    setSelectedItem: () => {}
  }
  );