import { useContext } from "react";
import { SelectCategory } from "../../App.context";

const CloseButton = () => {
  const { setSelectedItem } = useContext(SelectCategory);
  
  return (
    <div className="flex items-center" onClick={() => setSelectedItem(null)}>
      <div className="h-16">
        <div className="bg-amber-200 px-1 h-16 w-[28px] flex justify-center items-center rounded-tr-xl rounded-br-xl skew-y-12">
        </div>
        <div className="bg-amber-200 px-1 h-16 w-[28px] relative -top-10 flex justify-center items-center rounded-tr-xl rounded-br-xl -skew-y-12">
        </div>
        <div className="bg-amber-200 px-1 h-16 relative -top-[117px] flex justify-center items-center rounded-tr-xl rounded-br-xl ">
          <img className="w-5 h-5" src={require(`../../assets/activo.png`)}/>
        </div>
      </div>
    </div>
  )
}

export default CloseButton
