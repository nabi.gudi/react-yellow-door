const Item = ({ item } : { item: {img?: string, name: string} }) => {
  return (
    <div className="flex flex-col items-center">
      {item.img ?
        <img className="w-auto h-18 aspect-square" src={item.img}/>
      : 
        <div className="w-full h-24 bg-gray-400"></div>
      }
      <span className="text-[0.75vw] text-gray-800 w-auto mt-1">{item.name}</span>
    </div>
  )
}

export default Item
