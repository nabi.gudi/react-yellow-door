import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { SelectCategory } from "../../App.context";
import CloseButton from "./CloseButton";
import Item from "./Item"
import Subsection from "./Subsection";

interface Item {
  name: string,
  img: string
}

interface SectionProps {
  name: string,
  items: Item[]
}

const Section = ({menu} : {menu: string}) => {
  const [sections, setSections] = useState<SectionProps[]>([{name: "", items:[{name: "", img: "",}]}]);

  const { setSelectedItem } = useContext(SelectCategory);

  const [subsectionSelected, setSubsectionSelected] = useState<number|null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const url = 'https://us-central1-prueba-front-280718.cloudfunctions.net/'.concat(menu.toLowerCase());

  const clearData = () => {
    setSections([{name: "", items:[{name: "", img: "",}]}]);
    setSubsectionSelected(null);
  }
  useEffect(() => {
    setLoading(true);
    clearData();
    axios.get(url)
    .then(({ data })=> {
      setSections(data);
      setLoading(false);
    })
    .catch((err)=> {
      console.error(err)
      setLoading(false);
    })
  }, [menu]);

  return (
    <div className="flex flex-row items-center">
      <div className="flex flex-col items-start top-0 w-[30vw] h-full p-5 bg-amber-200 ">
        <div className="flex flex-row items-center pb-2"  onClick={() => setSelectedItem(null)}>
          <img className="w-3 h-3" src={require(`../../assets/activo.png`)}/>
          <span className="text-xs text-gray-800 ">{menu}</span>
        </div>
        {loading && <span className="text-md mt-2">Cargando...</span>}
        
        {
          subsectionSelected === null && !loading && sections.map((section, index) => {
            return (
              <div key={index} className="w-full bg-amber-500 my-3 py-2 px-5 rounded-xl flex flex-row justify-between items-center" onClick={() => setSubsectionSelected(index)}>
                <h1 className="text-md">{section.name}</h1>
                <img className="w-4 rotate-180" src={require(`../../assets/activo.png`)}/>
              </div>
            )
          })
        }
        {subsectionSelected != null && !loading &&
          <Subsection section={sections[subsectionSelected].name} subsections={sections[subsectionSelected].items}/>
        }
      </div>
      <CloseButton />
    </div>
  )
}

export default Section
