import Item from "./Item"

interface Item {
  name: string;
  img: string;
}

interface SectionProps {
  name: string;
  items: Item[];
}

const Subsection = ({section, subsections} : {section: string, subsections: Item[]}) => {
  console.log(section, subsections);
  return (

    <div className="flex flex-col items-start w-full">
      <h1 className="text-lg font-bold mb-3">{section}</h1>
      <div className="flex flex-col items-start top-0 w-full h-full p-5 ">
        <div className="grid grid-cols-3 gap-5 w-full h-auto">
        {
          subsections.map((item: {name: string, img: string }, index) => <Item  key={index} item={item}/> )
        }
        </div>
      </div>
    </div>
  )
}

export default Subsection
