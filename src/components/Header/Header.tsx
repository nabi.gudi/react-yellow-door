import Dropdown from "./Dropdown"

const Header = () => {
  const options = [
    { name: 'Guardar y Salir', unavailable: false },
    { name: 'Salir sin guardar', unavailable: false },
    { name: 'Guardar y continuar', unavailable: false },
  ]

  return (
    <div className="w-full h-16 bg-amber-100 flex flex-row justify-between items-center px-5 py-3">
      <img className="w-[180px] my-2" src={require(`../../assets/logo.png`)}/>
      <Dropdown options={options}/>
    </div>
  )
}

export default Header
