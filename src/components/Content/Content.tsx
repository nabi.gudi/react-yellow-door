const Content = () => {
  return (
    <div className="w-full h-full flex flex-col justify-between items-end ">
      <div className="mr-5 mt-5">
        <button className="bg-white rounded-xl py-2 px-10 " onClick={() => console.log('Button Fijar clicked')}>Fijar</button>
        <button className="bg-white rounded-xl py-2 px-10 ml-5" onClick={() => console.log('Button Borrar clicked')}>Borrar</button>
      </div>
      <div className="flex flex-row mr-5 mb-5">
        <div className="flex flex-col bg-white rounded-xl mr-3">
          <button className=" py-2 px-1 " onClick={() => console.log('Button + clicked')}>
            <img className="w-6" src={require(`../../assets/plus.png`)}/>
          </button>
          <button className="py-2 px-1 " onClick={() => console.log('Button - clicked')}>
            <img className="w-6" src={require(`../../assets/minus.png`)}/>
          </button>
        </div>
        <div className="bg-white rounded-xl py-2">
          <div className="h-6">
            <button className="px-4 " onClick={() => console.log('Button arrow up clicked')}>
              <img className="w-6 h-6" src={require(`../../assets/Flecha.png`)}/>
            </button>
          </div>
          <div className="flex flex-row">
            <button className="pl-4 pr-2 " onClick={() => console.log('Button arrow left clicked')}>
              <img className="w-6 h-6 -rotate-90" src={require(`../../assets/Flecha.png`)}/>
            </button>
            <button className="pr-4 pl-2" onClick={() => console.log('Button arrow right clicked')}>
              <img className="w-6 h-6 rotate-90" src={require(`../../assets/Flecha.png`)}/>
            </button> 
          </div>
          <div className="h-6">
            <button className="px-4 " onClick={() => console.log('Button arrow down clicked')}>  
              <img className="w-6 h-6 rotate-180" src={require(`../../assets/Flecha.png`)}/>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Content
