import Item from "./Item"

const Sidebar = ({sections}: {sections: string[]}) => {
  let newSections = [""].concat(sections).concat("");

  return (
    <div className="w-[7vw]">
      <div className="w-[7vw] bg-amber-300  h-full flex flex-col justify-center">
        {newSections.map((section, index) => <Item key={index} section={section} index={index} selectible={index !== 0 && index != newSections.length - 1} />)}
      </div>
    </div>
  )
}

export default Sidebar