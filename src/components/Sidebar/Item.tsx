import { useContext } from "react";
import { SelectCategory } from "../../App.context";

interface ItemProps {
  section: string, 
  index: number, 
  selectible: boolean
}

const Item = ({section, index, selectible} : ItemProps) => {

  const { selectedItem, setSelectedItem } = useContext(SelectCategory);

  return (
    <div className="bg-amber-500">
      <div className={`flex flex-col items-center py-3 ${selectedItem && index === selectedItem ? 'bg-amber-500' : 'bg-amber-300'}  ${selectedItem && index === selectedItem -1 && 'rounded-br-3xl'}  ${selectedItem && index === selectedItem + 1 && 'rounded-tr-3xl'} ${selectible && 'hover:text-red-600'}`} onClick={() => selectible && setSelectedItem(index)}>
        {section ?
        <img className="w-8" src={require(`../../assets/${section}.png`)}/>
        :
          <div className="w-8 h-16"></div>
        }

        <span className="text-[0.75vw] text-gray-800 w-auto">{section}</span>
      </div>
    </div>
  )
}

export default Item
